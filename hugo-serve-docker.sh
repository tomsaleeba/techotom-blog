#!/bin/bash
set -euo pipefail
cd "$(dirname "$0")"

docker run \
  --rm \
  -it \
  -v "$PWD:/src:ro" \
  -v "$PWD/.hugo_build.lock:/src/.hugo_build.lock:rw" \
  -v "$PWD/resources:/src/resources:rw" \
  -u "$(id -u):$(id -g)" \
  -p 1313:1313 \
  `#klakegg/hugo:latest-ext serve -D` \
  registry.gitlab.com/pages/hugo/hugo_extended hugo server --bind 0.0.0.0
