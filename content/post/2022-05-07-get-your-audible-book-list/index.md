---
title: "Get your Audible book list"
date: 2022-05-07
summary: How to get a list of your Amazon Audible library as text
tags:
- books
- javascript
---

Edit Feb 2023: I found [https://github.com/joonaspaakko/audible-wishlist-scraper]() which
has more features, but is potentially harder to audit if you're wary of running code in
your browser console.

# Summary
I read this post [https://petargyurov.com/bookshelf/]() ([HN
comments](https://news.ycombinator.com/item?id=31293727)) and it motived me to
stash the list of books I've listened to on Audible.

This approach requires you to
1. login to your Audible account on their webapp
1. go to your Library
1. set the page size to as large as it can go
1. open the devtools in your browser
1. run a snippet of JavaScript

Here's the JavaScript:
```js
;(() => {
  const rows = [...document.getElementsByClassName('adbl-library-content-row')]
  console.log(`Found ${rows.length} rows`)
  const books = rows.reduce((accum, curr) => {
      const author = curr.querySelector('.authorLabel .bc-size-callout').textContent
      const title = curr.querySelector('.bc-size-headline3').textContent
      accum.push({
          author,
          title,
          full: `${title} -- ${author}`,
          })
      return accum
      }, [])
  const booklistStr = books.map(b => b.full).join('<br />')
  const theHtmlStr = `
    <dialog open style="z-index: 99999; position: fixed;">
      <p style="overflow-y: scroll; max-height: 90vh; max-width: 80vw;">${booklistStr}</p>
      <form method="dialog">
        <button>OK</button>
      </form>
    </dialog>`
  const body = document.getElementsByTagName('body')[0]
  body.insertAdjacentHTML('afterbegin', theHtmlStr)
})()
```

This is what the result looks like, an HTML dialog:

<img src="./dialog.png" style="max-width: 80vw; max-height: 80vh;">

Then you can copy all the text from that `<dialog>` and save it somewhere.

Unfortunately it can only run on one page, because navigating to other pages is
a page navigation (JS can't keep running). I tried adjusting the `?pageSize=50`
querystring param to a larger value, but the page ignores it.

So you need to repeat this process for each page.
