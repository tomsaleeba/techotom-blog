---
title: "Grab Mixcloud URLs"
date: 2023-01-16
summary: "JS snippet to run in the browser"
tags:
- js
---

If you're trying to create a list of bookmarks for your favourite songs in Mixcloud, you
might want to save the URLs for a whole page of mixes.

You can do that by open the devtools console of your browser and running this snippet of
JS:
```js
[...document.querySelectorAll('img')]
  .filter(e => e.width === 144)
  .reduce((accum,curr) => {
    accum += `${curr.parentElement.parentElement.href}\n`
    return accum
  }, '')
```
