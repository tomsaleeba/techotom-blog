---
title: "Manually handle custom XDG protocol callback for Linux apps needing auth callbacks"
date: 2024-02-06
summary: "get Location header from resp in browser and call app with `$app --open-url $url`"
tags:
- xdg
- linux
---

In my case I'm trying to use plugin for `vscode` that requires you being signed in to an
account. `vscode` can trigger the login window in the browser, but the callback from the
browser to `vscode` doesn't work because I don't have an XDG protocol handler configured
for `vscode://` URLs due to the way I installed `vscode` (with flatpak).

# Option 1: permanent fix
Register an XDG handler for the protocol: https://stackoverflow.com/a/77670791/1410035.

# Option 2: one-off workaround
We can manually one-off pass the login callback to vscode without configuring anything
long-term:
- trigger the login page, e.g. open vscode and click the "login" button that the plugin
  shows, which will open your browser
- before you login, open your browser devtools
- do the login
- look for the HTTP request that is failing with "an unknown protocol" (it's complaining
  about `vscode://`)
- inspect the response for that request and you should see a `Location` header
- copy the URL value of that `Location` header (referred to as `$THAT_URL`)
- in your terminal, run
    ```bash
    vscode --open-url "$THAT_URL"
    ```

By doing this, we've recreated what a registered protocol handler would do.

Extra detail: as I'm using Flatpak, the auth was lost each time I closed vscode because
the filesystem changes weren't persistent. To fix that, I:
- opened `vscode`
- found the running "Instance" with `flatpak ps`
- entered the running flatpak sandbox with `flatpak enter $InstanceID sh`
- looked at the created directories in the home dir `cd ~; ls -la`
- for each of those dirs, use [flatseal](https://github.com/tchx84/Flatseal) to make those
  dirs Persistent
