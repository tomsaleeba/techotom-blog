---
title: "Moving an AWS Lambda between subnets"
date: 2021-12-30
summary: You also have to publish a new version and delete all the old versions
tags:
- aws
---

# Summary
One of my VPC subnets filled up, so I had to move my Lambda functions into a new
subnets, with more space. ENIs (Elastic Network Interfaces) that are
automatically allocated for Lambda functions were taking up 50 IPs in my subnet
and I needed them to be removed so other services using the old subnet would
have free IPs available. Just updating the subnet values for all the Lambdas
didn't seem to be enough. There are extra steps you need to do:

- publish a new version of the Lambda
- if you have aliases, update them to point to the new version
- delete all the old Lambda versions

Use the [bash
script](https://github.com/awslabs/aws-support-tools/blob/master/Lambda/FindEniMappings/findEniAssociations)
(more detail below) to find Lambda versions you need to delete.

# More detail
A useful tool is mentioned in this AWS ["Why can't I detach or delete an elastic network interface that Lambda
created"](https://aws.amazon.com/premiumsupport/knowledge-center/lambda-eni-find-delete/)
support article. The tool is this bash script:
https://github.com/awslabs/aws-support-tools/blob/master/Lambda/FindEniMappings/findEniAssociations.

The script finds Lambdas (specifically versions of Lambda functions) that are
still using the subnet and security group, which prevents the ENI from being
cleaned up.

After I'd already updated all the Lambdas to use the new subnets (without doing
the extra steps mentioned above), the script listed all the versions still
keeping ENIs in the old subnet alive. This is when I figured out I need to
publish new versions and delete the old ones. I don't know if the subnet changes
take effect when you save the changes or only once you publish a new version.
I'm guessing the latter though.

Anyway, I went through the annoying task of publishing a new version of every
Lambda, and deleting all the old versions. Then I ran the bash script again and
got the message:
> No manual changes to the ENI found. ENIs may take up to 20 minutes to be
> deleted. If this ENI is not deleted automatically in the next 24 hours then it
> may be 'stuck'. If IAM roles associated with a VPC Lambda function are deleted
> before the ENI is deleted, Lambda will not be able to complete the clean-up of
> the ENI. If the ENI will not allow you to delete it manually after 24 hours
> then please contact AWS support and send them the output of this script.

I waited the 20 minutes and the ENIs were in fact deleted, yay! Some interesting
points:
- I had 50 ENIs allocated to that subnet+security group. The was from ~30 Lambda
    functions (some with multiple versions). As I was deleting Lambda versions,
    I kept checking if the behind-the-scenes magic logic would scale down the
    number of ENIs allcated, but it didn't seem to. It wasn't until all Lambda
    versions were gone and then all ENIs were cleaned up. But then I also only
    waited for 10s of minutes and maybe it takes longer than that.
- the descriptions for the ENIs is misleading. It seems like when you first
    create a lambda and the ENI is allocated, it'll mention the name of the
    Lambda. However, if you then add more Lambda with the same subnet+security
    group combo and delete the original...the description of the ENI won't be
    updated, so it'll be misleading.
