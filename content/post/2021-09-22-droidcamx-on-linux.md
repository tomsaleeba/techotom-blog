---
title: "DroidCamX on Linux"
date: 2021-09-22T12:55:32-06:00
summary: Using DroidCamX to save and view streamed video in the command line.
tags:
- linux
---

I've been using [DroidCamX](https://www.dev47apps.com/) to stream video from my
phone onto my desktop. I can screenrecord my desktop and I don't have to muck
around with video editing; it's all composed and done in one take.

I was recording my keyboard so I needed the lowest latency possible and using
USB does much better than over WiFi. There are official native apps or you can
(apparently) use other tools like VLC or `ffmpeg`. Here's what I found to work
for me:

- make sure USB debugging is enabled on your phone
- plug your phone in via USB
- start DroidCamX app on phone
-
  ```
  adb forward tcp:4747 tcp:4747
  ```
- make sure your phone screen is on. If it's gone to sleep, you might not be
    able to connect
-
  ```
  curl 'http://127.0.0.1:4747/mjpegfeed?1920x1080' \
    | tee somefile.mp4 \
    | mpv - --no-cache --untimed --no-demuxer-thread
  ```

That last command deserves some explanation. We `curl` the 4747 port on our
local machine, which is forwarded to the DroidCamX app via ADB. We use a query
string to control the size of the video. You can find the available values for
this by:
- opening the web interface for DroidCam
- picking from the available resolutions
- clicking the info button to see the URL to use (or you can guess what the URL
  would look like)

We pipe that into `tee` so we write the video to a file. It's good to be able to
see what you're recording too so we pipe into `mpv`, which will show the video.
The params we pass to `mpv` make it low latency (no caching/buffering) and tell
it that it's a stream so don't confuse it with a standalone video file.

To stop recording, either `ctrl+c` the command or close `mpv`.
