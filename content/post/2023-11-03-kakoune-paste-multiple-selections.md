---
title: "kakoune paste block or multiple selections"
date: 2023-11-03
summary: "make sure you split the selection into lines before cutting"
tags:
- editors
- kakoune
---

Assume you have:

```
aaa
bbb
ccc

111
222
333
```

...and you want to end up with:

```
aaa   111
bbb   222
ccc   333
```

I dunno what you'd call this. Maybe "pasting a block" or "interlacing lines".

In vim (what I know fairly well), I'd do:

- position cursor on the first `1`
- `<c-v>` for block mode
- `j` twice to go down two lines
- `E` or `$` to extend selection to end
- `d` to cut (kill) the selection
- position cursor on end of the `aaa` line
- `<c-v>jj` to select a block of the last char of all 3 lines
- `A` to append
- insert four spaces
- `esc` to let the block insert finish
- `$` to jump to the end of the line
- `<c-v>jj` to select a block of the last char of all 3 lines
- `p` to paste the block (knowing the selected chars will be clobbered)

So as I'm still learning `kak`, I wanted to recreate that in kakoune. This is what I've
come up with:

- position cursor on the first `1`
- `]p` to select paragraph
- `<a-s>` to split on line boundaries
- `d` to cut (kill) the selections
- position cursor on the start of the `aaa` line
- `C` twice to add cursors on the two lines below
- `gl` to go-to end of line
- `p` to paste
- `k` to go up one line
- `<a-j>` to join lines
- `a` to append
- insert 2 spaces

Note: if you do *not* do the `<a-s>` to split the selection, you'll end up pasting the
whole block (all 3 lines) after each selected line. That's not what we want.

Or, I can do it without cutting the newlines:

- position cursor on the first `1`
- `C` twice to add cursors on the two lines below
- `x` to expand selection to the whole lines
- `_` to drop newlines
- `d` to cut (kill) selections
- position cursor on the start of the `aaa` line
- `C` twice to add cursors on the two lines below
- `gl` to go-to end of line
- `a` to append
- insert 3 spaces
- `<c-r>"` to paste register
- `esc` to exit insert mode

I'm a total `kak` n00b so I'm sure there are better ways, and I even found that `_` trick
to drop newlines while I was writing this, but I'm just happy I've got it working at all.
