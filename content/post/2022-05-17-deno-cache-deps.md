---
title: "Caching your deno deps in a specified directory"
date: 2022-05-17
summary: "Using `deno cache` and the `DENO_DIR` env var"
tags:
- deno
- js
---

Assume you have a `webserver.js` script with the following contents

```js
import { serve } from "https://deno.land/std@0.139.0/http/server.ts"

const port = 38080

// ...the rest of the code
```

You're coming from the NodeJS world and you're used to `node_modules/` in the
project directory. Deno caches dependencies, but not in the project directory.
They're in a location like `~/.cache/deno`.

You think is a good thing to have your dependencies cached in a directory in the
project, so let's get that working.

You do it with the `DENO_DIR` env var. So to have all the deps for our script
(above) stored in a directory in the project, we can do

```bash
DENO_DIR=$PWD/deps-cache deno cache webserver.js
```

Note that the directory doesn't need to already exist, it'll be created. That
previous command will produce a dir structure something like (truncated to be
easier to read)
```
$ tree deps-cache
deps-cache
├── deps
│   └── https
│       └── deno.land
│           ├── 0c58cb73b5d6da4c2fedcaa1824342eb8f99846672923517cafbed8589584a49
│           ├── 0c58cb73b5d6da4c2fedcaa1824342eb8f99846672923517cafbed8589584a49.metadata.json
│           ...
└── gen
    ├── file
    │   └── tmp
    │       └── tmp.SgmiznSvYm
    │           └── webserver.js.buildinfo
    └── https
        └── deno.land
            ├── 0c58cb73b5d6da4c2fedcaa1824342eb8f99846672923517cafbed8589584a49.js
            ├── 0c58cb73b5d6da4c2fedcaa1824342eb8f99846672923517cafbed8589584a49.meta
            ...
```

And, we can run our script using this same cache by using the same env var to
the `run` command:

```bash
DENO_DIR=$PWD/deps-cache deno run webserver.js
```

You might also want to checkout
- [deno compile](https://deno.land/manual/tools/compiler)
- [deno bundle](https://deno.land/manual/tools/bundler)
