---
title: "Boot Arch linux in Qemu after archinstall"
date: 2021-08-15T15:13:31-06:00
summary: "How to install grub to make your vm boot"
tags:
- linux
- arch
- qemu
---

### Q: How can I make my qemu VM boot after using `archinstall`?
Install `grub` and configure it. Run the `archinstall` process as normal, but at
the end, when asked if you want to `chroot` in, answer yes. Then you can install
grub:
```
pacman -S grub
```
...and then configure it:
```
grub-install --target=i386-pc /dev/vd1
grub-mkconfig -o /boot/grub/grub.cfg
```

See the [Arch wiki page for
grub](https://wiki.archlinux.org/title/GRUB#Installation) to read more about it.

Now you should be able to `reboot` and you'll have a booting system.

## More details

I think `archinstall` expects UEFI, as it should these days, but it seems `qemu`
won't boot UEFI out of the box (don't quote me, I'm a bit of n00b).

If you want to book with UEFI, I found some instructions that talk about using
`ovmf` to achieve that:
https://www.ubuntubuzz.com/2021/04/how-to-boot-uefi-on-qemu.html.

I also saw in the `virt-manager` UI that it has the ability to direct boot a
kernel, which seems like a viable option too. It's not one I researched though.

I've used Manjaro for quite a while and really like it. As the meme goes, I
guess I was too scared to install Arch so I thought I should give it a go after
readying about the financial concerns with Manjaro:
- https://archived.forum.manjaro.org/t/change-of-treasurer-for-manjaro-community-funds/154888
- https://www.reddit.com/r/linuxquestions/comments/i7tog4/whats_the_drama_happening_with_linux_manjaro_and/
I'm not sure how I feel about that stuff other than "a bit concerned". Manjaro
really has been a "it just works" experience for me so I'm not keen to give it
up.
