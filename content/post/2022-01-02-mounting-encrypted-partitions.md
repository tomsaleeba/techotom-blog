---
title: "Mounting encrypted Linux partitions from a Live Disc"
date: 2022-01-02
summary: Notes to my future self about how to deal with encrypted partitions
  when running in a Live Disc (or USB) environment
tags:
- linux
- dm-crypt
- arch
---

# Summary
I'm running Manjaro and recently had some issues booting. I have encrypted
partitions and haven't ever had to figure out how to mount them manually from a
Live Disc environment. This blog is a braindump of notes I used to get things
working. I think I'm using dm-crypt directly, but could be wrong about that.

**FIXME this probably needs editing/expanding \/**

Once chroot-ed in, you can read the most recent boot logs
```
journalctl -b
```
...and if that gives
```
-- No entries --
```
...then you can find the right boot with:
```
journalctl --list-boots
journalctl -b -1
```

Have to scroll back past shutdown stuff, but found
```
Jan 08 18:14:58 tom-x1eg2 lightdm[3003]: Traceback (most recent call last):
Jan 08 18:14:58 tom-x1eg2 lightdm[3003]:   File "/sbin/prime-offload", line 33, in <module>
Jan 08 18:14:58 tom-x1eg2 lightdm[3003]:     sys.exit(load_entry_point('optimus-manager==1.4', 'console_scripts', 'prime-offload')())
Jan 08 18:14:58 tom-x1eg2 lightdm[3003]:   File "/sbin/prime-offload", line 22, in importlib_load_entry_point
Jan 08 18:14:58 tom-x1eg2 lightdm[3003]:     for entry_point in distribution(dist_name).entry_points
Jan 08 18:14:58 tom-x1eg2 lightdm[3003]:   File "/usr/lib/python3.10/importlib/metadata/__init__.py", line 919, in distribution
Jan 08 18:14:58 tom-x1eg2 lightdm[3003]:     return Distribution.from_name(distribution_name)
Jan 08 18:14:58 tom-x1eg2 lightdm[3003]:   File "/usr/lib/python3.10/importlib/metadata/__init__.py", line 518, in from_name
Jan 08 18:14:58 tom-x1eg2 lightdm[3003]:     raise PackageNotFoundError(name)
Jan 08 18:14:58 tom-x1eg2 lightdm[3003]: importlib.metadata.PackageNotFoundError: No package metadata was found for optimus-manager
Jan 08 18:15:00 tom-x1eg2 lightdm[3005]: Traceback (most recent call last):
Jan 08 18:15:00 tom-x1eg2 lightdm[3005]:   File "/sbin/prime-switch", line 33, in <module>
Jan 08 18:15:00 tom-x1eg2 lightdm[3005]:     sys.exit(load_entry_point('optimus-manager==1.4', 'console_scripts', 'prime-switch')())
Jan 08 18:15:00 tom-x1eg2 lightdm[3005]:   File "/sbin/prime-switch", line 22, in importlib_load_entry_point
Jan 08 18:15:00 tom-x1eg2 lightdm[3005]:     for entry_point in distribution(dist_name).entry_points
Jan 08 18:15:00 tom-x1eg2 lightdm[3005]:   File "/usr/lib/python3.10/importlib/metadata/__init__.py", line 919, in distribution
Jan 08 18:15:00 tom-x1eg2 lightdm[3005]:     return Distribution.from_name(distribution_name)
Jan 08 18:15:00 tom-x1eg2 lightdm[3005]:   File "/usr/lib/python3.10/importlib/metadata/__init__.py", line 518, in from_name
Jan 08 18:15:00 tom-x1eg2 lightdm[3005]:     raise PackageNotFoundError(name)
Jan 08 18:15:00 tom-x1eg2 lightdm[3005]: importlib.metadata.PackageNotFoundError: No package metadata was found for optimus-manager
```

Can verify with
```
optimus-manager --status
```
...which gets the same error.

The fix is to reinstall this package, which you could try with:
```
$ yay -S optimus-manager # WON'T WORK!!!
 -> error fetching optimus-manager: System has not been booted with systemd as init system (PID 1). Can't operate.
Failed to connect to bus: Host is down 
	 context: exit status 1
```
...but that won't work because it's just a chroot. Instead, boot into a "no GUI" session and reinstall there. Then restart.



# Mounting partitions
- boot Manjaro live image
- start terminal
- launch a new terminal with a sane font size (thanks https://faq.i3wm.org/question/5725/any-ideas-how-to-change-the-font-size-for-a-urxvt-terminal-opened-on-a-specific-display-or-workspace.1.html)
    ```
    urxvt -fn "xft:Bitstream Vera Sans Mono:pixelsize=22"
    ```
- find the root partition. I don't think I'm using LVM here because I don't need
    more than 4 partitions:
    ```
    sudo fdisk -l
    ```
- unlock/open that encrypted partition. Note the last param is just a unqiue
    name, it can be anything
    ```
    sudo cryptsetup open /dev/nvme1n1p2 theroot
    ```
- mount the unlocked partition
    ```
    sudo mkdir /mnt/root
    sudo mount /dev/mapper/theroot /mnt/root
    ```
- if you run two disks, and you want to mount that second one into the first,
    you do it like any other mount, after you've unlocked it (assuming it's also
    encrypted)
    ```
    sudo cryptsetup open /dev/nvme0n1p0 thesecond
    sudo mount /dev/mapper/thesecond /mnt/root/<path>
    ```
- at this point you can browse both partitions like normal, just `cd` you way
    around. If you want to chroot in, you'll need to mount some other things:
    thanks (https://github.com/FadeMind/mhwd-chroot/blob/master/mhwd-chroot-shell)
    ```
    for curr in /{proc,dev,sys,dev/pts,tmp}; do
      echo "mounting $curr"
      sudo mount -B $curr /mnt/root$curr
    done
    ```
- now we `chroot` in:
    ```
    sudo chroot /mnt/root
    ```

# Enabling grub menu
- make sure you're in the chroot from above
- vim /etc/default/grub
    ```
    GRUB_TIMEOUT_STYLE=menu
    GRUB_TIMEOUT=3
    ```
- update grub
    ```
    update-grub
    ```

# Boot to text-only mode
- Now you'll actually see the grub menu when booting, you can use the `e` to
    edit the boot command trick
- find the line with `vmlinuz` and add ` 3` to the end (go to the next line and
    press left to get to the end of that long line quickly)
- boot with `F10`
thanks https://archived.forum.manjaro.org/t/boot-in-pure-text-mode/21047

# Check boot logs
```
journalctl -b | less
```
