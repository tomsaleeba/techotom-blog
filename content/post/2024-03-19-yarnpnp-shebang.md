---
title: "Writing shebang for an executable js script with yarn v2+ and PnP"
date: 2024-03-19
summary: "use `env -S`"
tags:
- yarn
- javascript
- linux
---

The bells-and-whistles form that can be called from any dir is:

```
#!/usr/bin/env -S bash -c 'cd "$(dirname "$0")" && yarn exec zx "$(basename "$0")"'
```

If you don't need the ability to call the script from any dir, you can simplify to:

```
#!/usr/bin/env -S yarn exec zx
```
