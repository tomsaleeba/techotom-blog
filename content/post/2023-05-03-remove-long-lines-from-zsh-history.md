---
title: "How to tidy up your shell history by removing long commands"
date: 2023-05-03
summary: "Use the sed command in this post"
tags:
- zsh
- linux
---

From time to time I accidentally paste a bunch of crap, like a long base64 string, into my
terminal. It'd not part of a valid command, it's just the string and because it has a
trailing newline, the shell tries (and fails) to execute the command, which means the
"command" is now in my `.zsh_history`.

The annoying part comes when I'm fuzzy searching back through my history and these base64
strings match a whole heap of things. I want them gone.

This `sed` command works for me with `zsh` and using GNU sed:

```
sed -i '/;[^ ]\{100,\}$/d' ~/.zsh_history
# on macOS you'll probably have to give a param to -i
```

The lines in the file have a format of:
```
: <timestamp>:0;<command>
```

Example:
```
: 1662496419:0;ls -l
```

So the sed regex looks for a string at least 100 chars long that consists of anthing
except spaces until line end. Then it deletes that line.

The command isn't super robust but it works well enough for me.
