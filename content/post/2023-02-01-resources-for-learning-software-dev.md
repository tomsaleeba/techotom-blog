---
title: "Resources for learning software development"
date: 2023-02-01
summary: "A living post of things I find to up-skill your software dev skills."
tags:
- learning
- software development
---

# Exercism
[https://exercism.org/]()

> Develop fluency in 66 programming languages with our unique blend of learning, practice
> and mentoring. Exercism is fun, effective and 100% free, forever.

Good for all skills level.s

# Destroy all software 
[https://www.destroyallsoftware.com/screencasts]()

Screencasts from an experienced dev (Gary Bernhardt) on a range of topics and languages.

# Execute Program
[https://www.executeprogram.com/]()

Another Gary Bernhardt site with interactive questions (using spaced repitition) to learn
TypeScript and JavaScript.

# Roadmaps.sh
[https://roadmap.sh/roadmaps]()

> Step by step guides and paths to learn different tools or technologies

This does doesn't actually deliver the learning, but it's a roadmap so you can plan your
learning. It has links to resources you can use to learn each topic.

# FutureCoder.io
[https://futurecoder.io/]()

Learn python from scratch in an interactive (in browser) environment. Great for beginners.
