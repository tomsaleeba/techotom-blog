---
title: "Checking MIPS and 7z benchmark on a few AWS EC2 instances"
date: 2022-12-08
summary: "Seeing how Graviton compares with x86"
tags:
- linux
- aws
- benchmarking
---

You can read about the different EC2 instance types [in the official
doco](https://aws.amazon.com/ec2/instance-types/).

This post compares a few different ARM and x86 EC2 families. We look at two
things
- BogoMIPS
- 7zip benchmark, using a single core and using 2 cores

These numbers might not be "real world" but it gives you something to compare
the different families.

# BogoMIPS

From [Wikipedia](https://en.wikipedia.org/wiki/BogoMips):

> BogoMips (from "bogus" and MIPS) is a crude measurement of CPU speed made by
> the Linux kernel when it boots to calibrate an internal busy-loop.

One thing I found by comparing a `c6g.medium` to a `c6g.large` is that MIPS is
per-CPU. So having more CPUs attached to the instance *will not* change the
number. That's good news that we only have to compare families of instances.

## M5zn
```
$ lscpu
  Model name:            Intel(R) Xeon(R) Platinum 8252C CPU @ 3.80GHz
    BogoMIPS:            7599.99
```

## c6a
```
$ lscpu
  Model name:            AMD EPYC 7R13 Processor
    BogoMIPS:            5299.98
```

## c6g
```
$ lscpu
  Model name:            Neoverse-N1
    BogoMIPS:            243.75
```

## c6i
```
$ lscpu
  Model name:            Intel(R) Xeon(R) Platinum 8375C CPU @ 2.90GHz
    BogoMIPS:            5799.92
```

## c7g
```
$ lscpu
Vendor ID:               ARM
  BogoMIPS:              2100.00
```

# 7z benchmark

The instances are using these AMIs (Ubuntu 22.04)
```
ARM: ami-0e2b332e63c56bcb5
x86: ami-0574da719dca65348
```

The benchmark is using 7zip, looped 4 times. We configure it to use just one, or
both CPUs to see how that affects things.

The procedure after you create the instance is basically
```
sudo apt update
sudo apt -y install p7zip-full
lscpu
7z b -mmt1 4
7z b -mmt2 4
```

## single CPU

Here's a summary version, with full details below
```
                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS
# M5zn
Avr:             100   4858   4858  |              100   4490   4489
Tot:             100   4674   4673
# c6a
Avr:             100   4436   4435  |              100   4545   4545
Tot:             100   4490   4490
# c6g
Avr:             100   3202   3202  |              100   3423   3422
Tot:             100   3313   3312
# c6i
Avr:             100   4917   4917  |              100   3981   3981
Tot:             100   4449   4449
# c7g
Avr:             100   3479   3478  |              100   4134   4134
Tot:             100   3806   3806
```

### M5zn
```
ubuntu@ip-172-30-2-163:~$ 7z b -mmt1 4

7-Zip [64] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=C.UTF-8,Utf16=on,HugeFiles=on,64 bits,2 CPUs Intel(R) Xeon(R) Platinum 8252C CPU @ 3.80GHz (50657),ASM,AES-NI)

Intel(R) Xeon(R) Platinum 8252C CPU @ 3.80GHz (50657)
CPU Freq: - - - - 128000000 - - - -

RAM size:    7653 MB,  # CPU hardware threads:   2
RAM usage:    435 MB,  # Benchmark threads:      1

                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS

22:       5177   100   5037   5036  |      52669   100   4497   4497
23:       4780   100   4871   4871  |      51942   100   4497   4496
24:       4375   100   4704   4704  |      51199   100   4495   4495
25:       4071   100   4649   4649  |      50234   100   4471   4471
22:       5270   100   5128   5127  |      52648   100   4496   4495
23:       4726   100   4816   4816  |      52030   100   4504   4504
24:       4402   100   4734   4734  |      51255   100   4500   4500
25:       4106   100   4690   4689  |      49962   100   4448   4447
22:       5186   100   5046   5046  |      52735   100   4503   4503
23:       4740   100   4831   4830  |      51903   100   4493   4493
24:       4350   100   4678   4677  |      51082   100   4485   4485
25:       4035   100   4609   4608  |      50110   100   4461   4460
22:       5468   100   5320   5320  |      52645   100   4495   4495
23:       4921   100   5015   5015  |      51644   100   4471   4470
24:       4497   100   4836   4836  |      51414   100   4514   4514
25:       4173   100   4766   4765  |      50614   100   4505   4505
----------------------------------  | ------------------------------
Avr:             100   4858   4858  |              100   4490   4489
Tot:             100   4674   4673
```

### c6a
```
ubuntu@ip-172-30-2-76:~$ 7z b -mmt1 4

7-Zip [64] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=C.UTF-8,Utf16=on,HugeFiles=on,64 bits,2 CPUs AMD EPYC 7R13 Processor (A00F11),ASM,AES-NI)

AMD EPYC 7R13 Processor (A00F11)
CPU Freq: - - - - - - - - -

RAM size:    3821 MB,  # CPU hardware threads:   2
RAM usage:    435 MB,  # Benchmark threads:      1

                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS

22:       4891   100   4759   4759  |      53241   100   4546   4546
23:       4303   100   4385   4385  |      52585   100   4552   4552
24:       3927   100   4223   4223  |      51733   100   4542   4542
25:       3742   100   4273   4273  |      50547   100   4500   4499
22:       5159   100   5020   5020  |      53536   100   4571   4571
23:       4392   100   4476   4475  |      52759   100   4567   4567
24:       3892   100   4186   4185  |      51509   100   4522   4522
25:       3723   100   4251   4251  |      50674   100   4511   4510
22:       5044   100   4908   4908  |      53505   100   4569   4568
23:       4268   100   4350   4350  |      52737   100   4565   4565
24:       3925   100   4221   4221  |      51894   100   4556   4556
25:       3736   100   4267   4266  |      50938   100   4534   4534
22:       5000   100   4865   4865  |      53485   100   4567   4567
23:       4246   100   4327   4327  |      52625   100   4556   4555
24:       3927   100   4223   4222  |      51988   100   4565   4564
25:       3711   100   4238   4237  |      50518   100   4497   4496
----------------------------------  | ------------------------------
Avr:             100   4436   4435  |              100   4545   4545
Tot:             100   4490   4490
```

### c6g
```
ubuntu@ip-172-30-2-229:~$ 7z b -mmt1 4

7-Zip [64] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=C.UTF-8,Utf16=on,HugeFiles=on,64 bits,2 CPUs LE)

LE
CPU Freq: - - - - - - - 1024000000 -

RAM size:    3816 MB,  # CPU hardware threads:   2
RAM usage:    435 MB,  # Benchmark threads:      1

                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS
22:       3267   100   3179   3179  |      40408   100   3450   3450
23:       3120   100   3180   3180  |      39658   100   3433   3433
24:       2968   100   3192   3192  |      38978   100   3422   3422
25:       2840   100   3244   3244  |      38185   100   3399   3399
22:       3311   100   3222   3222  |      40483   100   3457   3456
23:       3103   100   3163   3162  |      39651   100   3432   3432
24:       2969   100   3193   3193  |      38884   100   3414   3414
25:       2841   100   3244   3244  |      38025   100   3385   3385
22:       3297   100   3208   3207  |      40436   100   3453   3452
23:       3075   100   3133   3133  |      39596   100   3428   3427
24:       2958   100   3181   3180  |      38913   100   3416   3416
25:       2828   100   3230   3230  |      38101   100   3392   3391
22:       3239   100   3151   3151  |      40429   100   3452   3452
23:       3165   100   3225   3225  |      39705   100   3437   3437
24:       3016   100   3243   3243  |      38866   100   3412   3412
25:       2847   100   3251   3251  |      37970   100   3380   3380
----------------------------------  | ------------------------------
Avr:             100   3202   3202  |              100   3423   3422
Tot:             100   3313   3312
```

### c6i
```
ubuntu@ip-172-30-2-69:~$ 7z b -mmt1 4

7-Zip [64] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=C.UTF-8,Utf16=on,HugeFiles=on,64 bits,2 CPUs Intel(R) Xeon(R) Platinum 8375C CPU @ 2.90GHz (606A6),ASM,AES-NI)

Intel(R) Xeon(R) Platinum 8375C CPU @ 2.90GHz (606A6)
CPU Freq: - - - - - - - - -

RAM size:    3843 MB,  # CPU hardware threads:   2
RAM usage:    435 MB,  # Benchmark threads:      1

                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS
22:       5809   100   5652   5651  |      46290   100   3953   3952
23:       5083   100   5180   5180  |      45974   100   3980   3980
24:       4251   100   4571   4571  |      45522   100   3997   3996
25:       3908   100   4463   4462  |      44811   100   3989   3988
22:       5748   100   5594   5592  |      46304   100   3954   3954
23:       4768   100   4859   4859  |      46026   100   3984   3984
24:       4231   100   4551   4550  |      45545   100   3999   3998
25:       3931   100   4490   4489  |      44848   100   3992   3992
22:       5600   100   5449   5448  |      46289   100   3952   3952
23:       5038   100   5134   5134  |      46004   100   3982   3982
24:       4299   100   4624   4623  |      45516   100   3996   3996
25:       3906   100   4461   4461  |      44831   100   3991   3990
22:       5641   100   5488   5488  |      46295   100   3953   3953
23:       5011   100   5106   5106  |      46038   100   3985   3985
24:       4282   100   4605   4605  |      45512   100   3996   3996
25:       3893   100   4445   4445  |      44849   100   3992   3992
----------------------------------  | ------------------------------
Avr:             100   4917   4917  |              100   3981   3981
Tot:             100   4449   4449
```

### c7g
```
ubuntu@ip-172-30-2-230:~$ 7z b -mmt1 4

7-Zip [64] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=C.UTF-8,Utf16=on,HugeFiles=on,64 bits,2 CPUs LE)

LE
CPU Freq: - - - - - - - - -

RAM size:    3816 MB,  # CPU hardware threads:   2
RAM usage:    435 MB,  # Benchmark threads:      1

                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS

22:       3620   100   3523   3522  |      49156   100   4197   4197
23:       3403   100   3468   3467  |      48047   100   4159   4159
24:       3229   100   3473   3472  |      46893   100   4117   4117
25:       3022   100   3452   3451  |      45549   100   4055   4054
22:       3657   100   3558   3558  |      49231   100   4204   4203
23:       3420   100   3486   3485  |      48025   100   4158   4157
24:       3227   100   3471   3470  |      46953   100   4122   4122
25:       3006   100   3433   3432  |      45598   100   4059   4059
22:       3621   100   3523   3523  |      49169   100   4198   4198
23:       3409   100   3474   3473  |      48101   100   4164   4164
24:       3233   100   3477   3477  |      46934   100   4121   4120
25:       3011   100   3438   3438  |      45504   100   4051   4050
22:       3626   100   3528   3528  |      49159   100   4198   4197
23:       3400   100   3465   3464  |      48067   100   4161   4161
24:       3234   100   3479   3478  |      46938   100   4121   4121
25:       2986   100   3410   3410  |      45682   100   4066   4066
----------------------------------  | ------------------------------
Avr:             100   3479   3478  |              100   4134   4134
Tot:             100   3806   3806
```

## two CPUs

Here's a summary version, with full details below
```
                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS
# M5zn
Avr:             184   4334   7963  |              200   3205   6406
Tot:             192   3769   7185
# c6a
Avr:             186   4342   8074  |              200   3700   7396
Tot:             193   4021   7735
# c6g
Avr:             184   3832   7068  |              200   3415   6825
Tot:             192   3624   6946
# c6i
Avr:             175   4318   7562  |              200   2752   5503
Tot:             188   3535   6532
# c7g
Avr:             185   5390   9989  |              200   4132   8258
Tot:             193   4761   9123
```

### M5zn
```
ubuntu@ip-172-30-2-163:~$ 7z b -mmt2 4
7-Zip [64] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=C.UTF-8,Utf16=on,HugeFiles=on,64 bits,2 CPUs Intel(R) Xeon(R) Platinum 8252C CPU @ 3.80GHz (50657),ASM,AES-NI)

Intel(R) Xeon(R) Platinum 8252C CPU @ 3.80GHz (50657)
CPU Freq: - - - - - - - - -

RAM size:    7653 MB,  # CPU hardware threads:   2
RAM usage:    441 MB,  # Benchmark threads:      2

                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS

22:       8032   174   4483   7814  |      73663   200   3145   6289
23:       7694   183   4276   7840  |      73024   200   3161   6321
24:       7466   188   4264   8028  |      72090   200   3165   6329
25:       7182   190   4316   8200  |      71485   200   3183   6363
22:       7991   173   4488   7774  |      74945   200   3202   6399
23:       7744   185   4266   7890  |      72996   200   3160   6319
24:       7465   188   4269   8027  |      72665   200   3190   6379
25:       7187   190   4322   8206  |      73801   200   3288   6569
22:       8072   175   4493   7853  |      76706   200   3280   6549
23:       7735   183   4301   7881  |      73574   200   3185   6369
24:       7483   188   4282   8047  |      72246   200   3172   6343
25:       7161   191   4284   8177  |      71074   200   3164   6326
22:       7929   172   4481   7714  |      75526   200   3227   6448
23:       7716   184   4274   7862  |      75038   200   3251   6495
24:       7420   188   4233   7979  |      75106   200   3299   6594
25:       7114   188   4313   8123  |      71967   200   3205   6406
----------------------------------  | ------------------------------
Avr:             184   4334   7963  |              200   3205   6406
Tot:             192   3769   7185
```

### c6a
```
ubuntu@ip-172-30-2-76:~$ 7z b -mmt2 4
7-Zip [64] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=C.UTF-8,Utf16=on,HugeFiles=on,64 bits,2 CPUs AMD EPYC 7R13 Processor (A00F11),ASM,AES-NI)

AMD EPYC 7R13 Processor (A00F11)
CPU Freq: - - - - - - - - -

RAM size:    3821 MB,  # CPU hardware threads:   2
RAM usage:    441 MB,  # Benchmark threads:      2

                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS

22:       7996   173   4488   7779  |      86818   200   3709   7413
23:       7834   188   4241   7982  |      86634   200   3755   7499
24:       7492   190   4230   8055  |      83650   200   3672   7344
25:       7381   191   4421   8428  |      82311   200   3664   7326
22:       8088   172   4571   7869  |      86631   200   3699   7397
23:       7852   188   4262   8001  |      85623   200   3706   7412
24:       7546   190   4263   8113  |      84151   200   3695   7388
25:       7381   191   4402   8428  |      82474   200   3671   7341
22:       8159   179   4435   7938  |      86581   200   3697   7392
23:       7801   189   4207   7949  |      86088   200   3731   7452
24:       7552   191   4257   8120  |      83671   200   3673   7346
25:       7343   193   4353   8385  |      82166   200   3658   7313
22:       7861   171   4479   7647  |      87214   200   3726   7446
23:       7848   190   4212   7997  |      85382   200   3696   7391
24:       7578   191   4276   8149  |      85972   200   3780   7548
25:       7312   191   4373   8350  |      82444   200   3671   7338
----------------------------------  | ------------------------------
Avr:             186   4342   8074  |              200   3700   7396
Tot:             193   4021   7735
```

### c6g
```
ubuntu@ip-172-30-2-229:~$ 7z b -mmt2 4

7-Zip [64] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=C.UTF-8,Utf16=on,HugeFiles=on,64 bits,2 CPUs LE)

LE
CPU Freq: - - - - - - - - -

RAM size:    3816 MB,  # CPU hardware threads:   2
RAM usage:    441 MB,  # Benchmark threads:      2

                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS

22:       6689   174   3734   6507  |      80753   200   3448   6895
23:       6800   184   3760   6929  |      79341   200   3435   6868
24:       6671   188   3806   7173  |      77340   200   3398   6790
25:       6490   187   3973   7411  |      75823   200   3379   6749
22:       7025   182   3765   6835  |      80755   200   3448   6895
23:       6898   183   3831   7028  |      79062   200   3425   6844
24:       6688   188   3825   7191  |      77722   200   3415   6823
25:       6463   186   3976   7380  |      75599   200   3370   6729
22:       7034   178   3839   6843  |      80684   200   3447   6889
23:       6926   187   3782   7057  |      79276   200   3432   6862
24:       6721   188   3841   7227  |      77547   200   3405   6808
25:       6434   186   3948   7347  |      75923   200   3382   6758
22:       6958   180   3765   6770  |      80551   200   3441   6877
23:       6878   189   3710   7008  |      79146   200   3428   6851
24:       6570   185   3824   7064  |      77790   200   3415   6829
25:       6403   186   3935   7311  |      75688   200   3373   6737
----------------------------------  | ------------------------------
Avr:             184   3832   7068  |              200   3415   6825
Tot:             192   3624   6946
```

### c6i
```
ubuntu@ip-172-30-2-69:~$ 7z b -mmt2 4

7-Zip [64] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=C.UTF-8,Utf16=on,HugeFiles=on,64 bits,2 CPUs Intel(R) Xeon(R) Platinum 8375C CPU @ 2.90GHz (606A6),ASM,AES-NI)

Intel(R) Xeon(R) Platinum 8375C CPU @ 2.90GHz (606A6)
CPU Freq: - - - - - - - - -

RAM size:    3843 MB,  # CPU hardware threads:   2
RAM usage:    441 MB,  # Benchmark threads:      2

                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS

22:       7593   164   4516   7387  |      63876   200   2728   5454
23:       7283   173   4301   7421  |      63292   200   2740   5479
24:       7100   181   4209   7634  |      63158   200   2773   5545
25:       6924   186   4247   7907  |      61898   200   2755   5509
22:       7458   161   4516   7255  |      63958   200   2732   5461
23:       7267   173   4270   7405  |      63559   200   2751   5502
24:       7110   180   4237   7645  |      63512   200   2791   5576
25:       6844   188   4159   7815  |      61939   200   2758   5513
22:       7495   160   4564   7291  |      63797   200   2724   5447
23:       7338   171   4379   7478  |      63340   200   2742   5483
24:       7121   181   4229   7657  |      62846   200   2759   5517
25:       6927   187   4235   7910  |      61916   200   2756   5511
22:       7469   162   4498   7266  |      64591   200   2760   5515
23:       7258   174   4256   7396  |      63366   200   2743   5485
24:       7100   181   4224   7634  |      62810   200   2757   5514
25:       6915   186   4243   7895  |      62136   200   2767   5531
----------------------------------  | ------------------------------
Avr:             175   4318   7562  |              200   2752   5503
Tot:             188   3535   6532
```

### c7g
```
ubuntu@ip-172-30-2-230:~$ 7z b -mmt2 4

7-Zip [64] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=C.UTF-8,Utf16=on,HugeFiles=on,64 bits,2 CPUs LE)

LE
CPU Freq: - - - - - - - - -

RAM size:    3816 MB,  # CPU hardware threads:   2
RAM usage:    441 MB,  # Benchmark threads:      2

                       Compressing  |                  Decompressing
Dict     Speed Usage    R/U Rating  |      Speed Usage    R/U Rating
         KiB/s     %   MIPS   MIPS  |      KiB/s     %   MIPS   MIPS

22:      10306   186   5389  10026  |      98213   200   4198   8385
23:       9693   186   5301   9877  |      96001   200   4159   8310
24:       9155   182   5398   9844  |      93609   200   4114   8218
25:       8875   185   5465  10133  |      91158   200   4059   8114
22:      10287   186   5369  10008  |      98249   200   4198   8389
23:       9663   185   5318   9846  |      96075   200   4161   8316
24:       9189   183   5409   9881  |      93663   200   4114   8223
25:       8616   179   5495   9838  |      91087   200   4057   8107
22:      10407   188   5395  10125  |      98193   200   4194   8384
23:       9930   189   5345  10118  |      96068   200   4160   8316
24:       9221   183   5420   9915  |      93734   200   4116   8229
25:       8728   183   5447   9966  |      91115   200   4057   8110
22:      10344   187   5373  10063  |      98227   200   4198   8387
23:       9728   187   5307   9912  |      95987   200   4157   8309
24:       9176   183   5378   9867  |      93606   200   4114   8218
25:       9111   192   5429  10403  |      91159   200   4059   8114
----------------------------------  | ------------------------------
Avr:             185   5390   9989  |              200   4132   8258
Tot:             193   4761   9123
```
