---
title: "Running caddy for local HTTPS with self-signed/snake-oil cert"
date: 2024-02-06
summary: "generate cert+key, write a short Caddyfile, run caddy"
tags:
- caddy
- https
- linux
---

You have some HTTP service that listens on a port but doesn't have HTTPS/SSL/TLS. You want
to add HTTPS without touching the service. We'll do that using `caddy` as a reverse proxy
in front of your service, so all requests to caddy are forwarded to your service after
HTTPS termintaion is done.

You could go to the effort to get certificates that are trusted by the CA included in most
devices but it's less effort to use self-signed (AKA snake-oil, because they're not
trust-worthy. Lookup "snake-oil salesperson" for more on that) certificates. The downside
is you need to explicitly tell your tools to trust that certificate.

# Install caddy
Follow [the docs](https://caddyserver.com/docs/install).

# Make a dir for caddy stuff
```
mkdir ~/caddy-stuff
cd ~/caddy-stuff
```

# Generate a cert/key:
Note: we're using `blah.local` as the hostname, but you can also just use `localhost`.
```
openssl req \
  -x509 \
  -newkey rsa:4096 \
  -keyout key.pem \
  -out cert.pem \
  -sha256 \
  -days 365 \
  -nodes \
  -subj "/C=XX/ST=StateName/L=CityName/O=CompanyName/OU=CompanySectionName/CN=blah.local"
# thanks https://stackoverflow.com/a/10176685/1410035
```

# Create a Caddyfile
Make a file name `Caddyfile` with the contents:
```
{
  http_port    8080
  https_port   8443
}

local.saplinglearning.me {
  tls ./cert.pem ./key.pem
  reverse_proxy :1234
}
```

We specify overridden http and https ports so you don't need superuser perms to bind to
ports <1000. If you remove those directives, the defaults of 80 and 443 will take effect.

The `:1234` directive is the address of your other service that we're proxying. This
syntax implies port `localhost:1234`. Change to suit the port you use.

# Start your downstream service
Whatever thing you're trying to add HTTPS to, start that service however you normally do it.

# Start caddy
```
caddy run
```

# Test it out
Now you can access your service via https:
```
curl -k https://blah.local:8443/whatever
```

You'll get warning about the self-signed cert, so for `curl` with use `-k` to ignore that.
In the browser, you'll have to accept the warning.
