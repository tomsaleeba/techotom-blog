---
title: "Minimising finger movement for keyboard keys"
date: 2021-11-18
summary: Non-functioning prototype of the finger part of a keyboard, with photos!
tags:
- keyboards
---

I'm basically just gonna dump some photos here. As usual, I'm thinking about
keyboards. Here are the requirements I'm trying to satisfy with this idea:

- minimise finger movement, as in the movement to get to a key, not the movement
    to activate the key.
- use off-the-shelf keyboard parts. If you want to go bespoke, you should
    totally check out the [lalboard](https://github.com/JesusFreke/lalboard). If
    you have one that you want to sell, hit me up. I'm keen to try one.
- still allow for pressing more than one key at a time. I'm not sure this design
    does very well at this, but it's technically possible.
- make it easy to remove your hand/take it "off the keyboard". For taking a
    drink, grabbing the mouse, scratching your butt, etc.

Imagine this idea replicated four time, once for each finger. Then add a thumb
cluster and you've got a keyboard that might be portable or at least will sit on
your desk and have your wrist in a more neutral position.

I'm building on [my previous post about the TP roll
keyboard](/post/2020-12-21-tp-roll-chorded-keyboard-concept/).

<div style="margin-left: 2em;">
  <a href="./20210215_230213.jpg" target="_blank">
    <img src="./20210215_230213.jpg" style="max-width: 80vw; max-height: 50vh;">
  </a>
  <figcaption style="margin-bottom: 3em;">Assembled, side-on</figcaption>
  <a href="./20210215_230219.jpg" target="_blank">
    <img src="./20210215_230219.jpg" style="max-width: 80vw; max-height: 50vh;">
  </a>
  <figcaption style="margin-bottom: 3em;">Assembled, front-on</figcaption>
  <a href="./20210215_230247.jpg" target="_blank">
    <img src="./20210215_230247.jpg" style="max-width: 80vw; max-height: 50vh;">
  </a>
  <figcaption style="margin-bottom: 3em;">Assembled, demo of use</figcaption>
  <a href="./20210215_225746.jpg" target="_blank">
    <img src="./20210215_225746.jpg" style="max-width: 80vw; max-height: 50vh;">
  </a>
  <figcaption style="margin-bottom: 3em;">Unfolded, front-on</figcaption>
  <a href="./20210215_225802.jpg" target="_blank">
    <img src="./20210215_225802.jpg" style="max-width: 80vw; max-height: 50vh;">
  </a>
  <figcaption style="margin-bottom: 3em;">Unfolded, side-on</figcaption>
  <a href="./20210215_225008.jpg" target="_blank">
    <img src="./20210215_225008.jpg" style="max-width: 80vw; max-height: 50vh;">
  </a>
  <figcaption style="margin-bottom: 3em;">Measurements to make it yourself</figcaption>
</div>
