---
title: "DIY layouts for Multiling O keyboard"
date: 2021-08-15T15:14:31-06:00
summary: "Truly doing it yourself seeing as the server-side magic seems broken"
tags:
- android
- keyboards
---

I recently found the [Multiling O
keyboard](https://play.google.com/store/apps/details?id=kl.ime.oh) Android
keyboard. It's a pretty amazing project that has A LOT of functionality packed
into it.

One the features is being able to create a DIY layout by passing in some JSON.
For instance, looking at the page for the [Bigkey DIY
layout](https://hanwriting.blogspot.com/2016/02/diy-layout-bigkey.html), you'll
see there's not much there except:

> To import this layout:
> 1. open ↓ URL \
>   http://goo.gl/jWrTXj
> 2. scroll down and tap "Apply"

If you click the minified link, you are taken to an appspot site with a URL that
starts with: `http://ok-layout.appspot.com/le.html?{%0A"title":"...`. The
response is a HTTP 500 so it's not happy about something. To be fair, the blog
post with the link was written 5 years ago. Also, in case the link minifier ever
disappears, at least I'll have an example of the full URL kept in this blog.

Lucky for us, all the details we need for the DIY layout are *in* the URL. We
just have to parse it a bit before we can use it in the app. Here's the full
query string from that URL:

```
{%0A"title":"QWERTY+Bigkey",%0A"onScreen":{%0A"main":[%0A"qwerti[4D:+[LEFT][UP][RIGHT][DOWN]][DEL]",%0A"asd+uyop",%0A+"+zfgjhkl",%0A"[SHIFT]+xcvbnm",%0A"[TOOL][ALTGR:,][SPACE][][][SYM:.][ENTER]"%0A],%0A"sym":[%0A"€$%25^%26*()_%3D",%0A"@%23[W.LEFT][TRANSLATE][PGUP][CUT][W.RIGHT][|←Del]",%0A+"%2B-\\[HOME][ALL][END]:;",%0A"'\"[123][COPY][PGDN][PASTE]!?",%0A"[TOOL][ALTGR:,][SPACE][][][SYM:.][ENTER]"%0A]%0A}%0A}
```

First, the `%0A` sequences can either be deleted or, if you care about human
readability, replaced with a newline. That gets us:

```json
{
"title":"QWERTY+Bigkey",
"onScreen":{
"main":[
"qwerti[4D:+[LEFT][UP][RIGHT][DOWN]][DEL]",
"asd+uyop",
+"+zfgjhkl",
"[SHIFT]+xcvbnm",
"[TOOL][ALTGR:,][SPACE][][][SYM:.][ENTER]"
],
"sym":[
"€$%25^%26*()_%3D",
"@%23[W.LEFT][TRANSLATE][PGUP][CUT][W.RIGHT][|←Del]",
+"%2B-\\[HOME][ALL][END]:;",
"'\"[123][COPY][PGDN][PASTE]!?",
"[TOOL][ALTGR:,][SPACE][][][SYM:.][ENTER]"
]
}
}
```

Now we just have to remove those two leading `+` symbols:
```
+"+zfgjhkl",
+"%2B-\\[HOME][ALL][END]:;",
```
...and we end up with something that's valid JSON and the app will happily
accept. Here's the final version, with some indenting added:

```json
{
  "title":"QWERTY+Bigkey",
  "onScreen":{
    "main":[
      "qwerti[4D:+[LEFT][UP][RIGHT][DOWN]][DEL]",
      "asd+uyop",
      "+zfgjhkl",
      "[SHIFT]+xcvbnm",
      "[TOOL][ALTGR:,][SPACE][][][SYM:.][ENTER]"
    ],
    "sym":[
      "€$%25^%26*()_%3D",
      "@%23[W.LEFT][TRANSLATE][PGUP][CUT][W.RIGHT][|←Del]",
      "%2B-\\[HOME][ALL][END]:;",
      "'\"[123][COPY][PGDN][PASTE]!?",
      "[TOOL][ALTGR:,][SPACE][][][SYM:.][ENTER]"
    ]
  }
}
```

To use this JSON:
1. on your phone, copy the JSON above to the clipboard
1. open the Multiling O keyboard (tap in a text field)
1. press and hold on the spacebar, and drag to the top right *Layouts* item
1. select the `[ + DIY ]` menu item
1. use the *Paste* button

It's worth noting that it's only the key layout that is configured by this. The
theme/styling is not affected as you can see from this screenshot that has the
new layout but the default styling:

![](./bigkey.png)

Themes are also configured in a similar way, but the content of the URL seems
like it would be harder for a human to craft by hand. I've captured a few
layouts and themes here so other interested people can use them as examples to
create their own:

- [layout samples](./layout-archive.txt)
- [theme samples](./theme-archive.txt)
