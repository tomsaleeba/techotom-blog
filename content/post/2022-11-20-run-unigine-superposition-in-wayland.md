---
title: "How to run Unigine Superposition in Wayland"
date: 2022-11-20
summary: "Force the use of xwayland"
tags:
- linux
- wayland
---

# TL;DR
Unset some force to force xwayland:
```
env -u WAYLAND_DISPLAY -u QT_QPA_PLATFORM ./Superposition
```

# The story
I'm on wayland (sway) and want to run the [Unigine
Superposition](https://benchmark.unigine.com/superposition/) benchmark, but when
I try to run it, I get:
```
$ ./Superposition
This application failed to start because it could not find or load the Qt platform plugin "wayland"
in "".

Available platform plugins are: xcb.

Reinstalling the application may fix this problem.
[1]    14239 IOT instruction (core dumped)  ./Superposition
```

I saw [Brodie's video on running a full X11 session in
xwayland](https://www.youtube.com/watch?v=8jmxhhdfSd8) and thought I could use
that to force Superposition to run in xwayland.

That *does* work but it turns out it's not needed. You can get away with
- forcing the command to use xwayland by removing the `WAYLAND_DISPLAY` env var
- removing the `QT_QPA_PLATFORM` env var so QT doesn't still think it's in
  Wayland
...which gives you a command like
```
env -u WAYLAND_DISPLAY -u QT_QPA_PLATFORM ./Superposition
```

Thanks to [this reddit
comment](https://www.reddit.com/r/swaywm/comments/jivv8n/comment/ga90583/) from
OneMoreTurn with the tip on how to force a command to use xwayland.
