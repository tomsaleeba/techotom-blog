---
title: "Maximising GPU memory available on Linux with optimus hardware"
date: 2021-08-08
summary: "FIXME"
draft: true
tags:
- linux
- gta5
---

$ ll /usr/lib/firefox/firefox
-rwxr-xr-x 1 root root 519K Jul 22 07:44 /usr/lib/firefox/firefox
$ file /usr/lib/firefox/firefox
/usr/lib/firefox/firefox: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 4.4.0, BuildID[sha1]=c69add75ded06c48d3f1d837eb6b2d8a345c0262, stripped
$ ll /usr/bin/firefox
-rwxr-xr-x 1 root root 45 Jul 22 07:44 /usr/bin/firefox
$ file /usr/bin/firefox
/usr/bin/firefox: POSIX shell script, ASCII text executable

When I run with optimus-manager on `nvidia` mode, everything uses the nvidia
card, and it's all smooth. GPU mem usage is 1500mb. When I run in `hybrid` mode,
the intel GPU is used and nvidia GPU mem usage is ~110mb.

I tried to `prime-run firefox` but it still ran like crap. Comparing after I
went back to nvidia mode, it seems that `which firefox` is a shell script and
the actual exec is in /usr/lib (see above).

FIXME try running `prime-run /usr/lib/firefox/firefox` and see if that works
FIXME do other FF profiles also use the GPU?
FIXME what does the Firefox.desktop file use?

Ideal situation would be to use hybrid mode and configure GTA launch command in
Steam to use prime-run. That way we have to maximum GPU mem for the game. I can
always run kitty and firefox with prime-run to get good performance. I can
either close firefox before gaming or restart it on the intel GPU. kitty has
such little usage that it's not worth worrying about.

Hopefully rockstar uses the intel GPU.
