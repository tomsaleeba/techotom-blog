---
title: "Comparing AWS Fargate cost to EC2"
date: 2023-03-07
summary: "You pay about 20% more for the luxury of using Fargate"
tags:
- aws
---

Fargate cost for `us-east-1` ([from here](https://aws.amazon.com/fargate/pricing/)):
```
per vCPU per hour	 $0.04048
per GB per hour	   $0.004445
```

EC2 costs for `us-east-1` ([from here](https://aws.amazon.com/ec2/pricing/on-demand/)).
I've only included x86, not ARM, because I can run x86 on Fargate so presumably that's
what it's quoting. Also, x86 is typically more expensive than ARM so it's in Fargate's
favour:
```
# c6a
2 vCPU/ 4GiB   	   $0.0765
# c6i
2 vCPU/ 4GiB   	   $0.085
```

Now, if we use the Fargate cost to calculate 2 vCPU and 4GiB memory, so we can compare:
```
2 * $0.04048 + 4 * $0.004445 = 0.09874
```

So that's 1.29x and 1.16x more expensive, respectively. That's not too bad seeing as last
time I did this check it was ~5x more expensive.
