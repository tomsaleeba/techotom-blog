---
title: "Change mouse sensitivity (not acceleration) in Linux"
date: 2021-08-15T15:14:31-06:00
summary: "How to decrease mouse sensitivity in Linux for an accurate cursor"
tags:
- linux
- xinput
---

When I got a trackball, I wanted to have a precision/accurate mouse mode that I
could toggle on. In my search, all I could find were settings to change the
acceleration, which is *not* the same thing.

In the end, I stumbled upon the `Coordinate Transformation Matrix` property of
`xinput`, which can be abused to get the results we want.

I don't fully understand how it works, but I assume we transform the inputs from
mouse to a matrix of a larger/smaller size so the input represents
smaller/larger cursor movements on screen. At least, that's how it seems to
work.

To set this property, use:
```
ctmVal=2.5
xinput set-prop "pointer:<replace with your device>" "Coordinate Transformation Matrix" 1, 0, 0, 0, 1, 0, 0, 0, $ctmVal
```

The larger you make the `ctmVal` variable, the more precision (smaller cursor
movement for larger mouse movement) you will get.

You can see the script I use to set this [in my
dotfiles](https://github.com/tomsaleeba/dotfiles/blob/master/config/i3/scripts/precision-deft-pro-toggle#L23).

Side note: you need to replace that `<replace with your device>` placeholder
with the name of your device from `xinput list`. As an example, I use `ELECOM
TrackBall Mouse DEFT Pro TrackBall` here.

This script runs `2.5` as my "normal" value. When coupled with a high-ish
acceleration, it suits me well. The script can also toggle to a super-precision
mode (value of `10`) using a button bound on the trackball.

The only known flaw with this system is sometimes the cursor jumps around the
screen. To replicate this bug:
1. use i3wm
1. make sure you have two monitors
1. place your cursor anywhere on screen A that is *not* the exact centre
1. jump to another i3 workspace on screen B
1. jump back to the workspace on screen A
1. you'll see i3 sets your cursor to the exact centre of screen A
1. move your cursor
1. the cursor will jump to where you last left it

It's a small thing and it doesn't bother me.
