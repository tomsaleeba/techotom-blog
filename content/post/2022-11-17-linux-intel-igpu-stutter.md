---
title: "Linux Intel iGPU stutter"
date: 2022-11-17
summary: "How to get a smooth desktop"
tags:
- linux
---

# TL;DR
The power saving on the iGPU is causing the lag, force the GPU to max frequency
all the time with
```
sudo intel_gpu_frequency --max
```
On Arch Linux you can get this tool from
[intel-gpu-tools](https://archlinux.org/packages/community/x86_64/intel-gpu-tools/).

# The story
I bought the [Morefine S600 Apex
Engineer](https://www.indiegogo.com/projects/morefine-s600-apex-engineer-the-fastest-minipc#/),
which runs an Intel i9-12900HK iGPU. It's my first system that *only* has an
Intel GPU, as my previous one had the Optimus setup that included an nVidia GPU.

I guess the fix I talk about here is relevant to any Intel iGPU that uses the
`i915` driver, but I'm not 100% sure on that.

The CPU has a lot of power but when running a display at 4K, I'd notice a slight
amount of lag (in desktop tasks, not 3D) all the time and sometimes it would
stutter/lag and get worse briefly.

I tried a bunch of changes including bumping the CPU TDP and tweaking the power
management profiles to make the CPU more "performance" oriented, but nothing
made a difference.

When I watched the iGPU load with `intel_gpu_top` I'd see the frequency drop to
single digit MHz numbers, which is pretty low when the max is 1300MHz. On a whim
I tried this command:
```
sudo intel_gpu_frequency --max
```
...to pin the iGPU frequency to it's max number and that was *the* fix!!!

It's a relief because the rest of the system performs great but this constant
slight lag was driving me nuts.

I don't know how to make the change permanent though, so you'll have to run the
command after each boot (or X11/wayland login).
