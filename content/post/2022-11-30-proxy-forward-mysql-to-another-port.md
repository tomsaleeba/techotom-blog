---
title: "Proxy/forward mysql to another port"
date: 2022-11-30
summary: "Use `socat`"
tags:
- linux
- mysql
---

# The story
`mysql` listens on 3306 by default. I'm trying to diagnose a network issue where
I can connect to ports 80 and 443 on a machine, but can't connect to mysql. So I
want mysql to listen on one of the known good ports, so I can try to connect to
it there.

## Forward the port
We could change the mysql config so it listens on another port, but this is just
a temporary test, so we'll do it an easier way.

First, you need to make sure you have `socat` installed:
```
sudo yum install socat
```

Then, we'll run `socat` with this command:
```
sudo -d -d socat tcp-l:80,fork,reuseaddr tcp:127.0.0.1:3306
```

This will create a TCP listener on port 80 (less than 1000, so you need to be
root) and will forward that to port 3306 on the same host. The `-d -d` means
we'll see some logging when connections are made.

## Connect on the new port
Now you should be able to connect to mysql on the forwarded port (`80`):

```
# note user=foo and pass=bar in this example
mysql -ufoo -pbar -h 11.22.33.44 --port 80
```

Note: if you're providing the password on the CLI (usually a bad idea) because
you just don't care, and you're using the short form of the param `-p` like
above, don't include an equals sign (`=`). As an example, this is wrong
`-p=bar`. That means the password starts with the `=` symbol. It looks funny
having no space and no `=`, but it works.

# The lesson
It turned out the VM simply needed the security group update to allow
connections from a new server. The debugging above is interesting, but massive
overkill for such a simple fix. I just didn't think to check the security
groups, whoops.
