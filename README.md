This is the source code for my personal blog: https://blog.techotom.com/

# Development

## Create a new post
```bash
hugo new post/$(date +'%Y-%m-%d')-some-post-title.md
```

## Start server
```bash
hugo server --buildDrafts
```

## Resize and EXIF-scrub all images in directory
```bash
for curr in *.jpg; do ../../../scrub-and-resize-image.sh $curr; done
```

## Emoji
We have emoji support. Use the [GitHub
codes][https://www.webfx.com/tools/emoji-cheat-sheet/] like `:blush:`.

# TODO
Might be nice to use a theme like "after dark"
- https://codeberg.org/vhs/after-dark
- https://vhs.codeberg.page/after-dark/
- https://github.com/nerdneilsfield/after-dark
- all the element (syntax highlighting, card, etc) are nice: https://vhs.codeberg.page/after-dark/feature/quick-install/
