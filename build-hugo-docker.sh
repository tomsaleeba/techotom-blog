#!/bin/bash
set -euo pipefail
cd "$(dirname "$0")"

docker run \
  --rm \
  -it \
  -v "$PWD:/src:ro" \
  -v "$PWD/.hugo_build.lock:/src/.hugo_build.lock:rw" \
  -v "$PWD/resources:/src/resources:rw" \
  -v "$PWD/public:/src/public:rw" \
  -u "$(id -u):$(id -g)" \
  registry.gitlab.com/pages/hugo/hugo_extended hugo

echo 'You can serve the built files with:'
echo '  zsh -c "cd public/ && caddy file-server -listen 0.0.0.0:30000"'
